package com.virjar.hermes.hermesadmin.controller;

import com.google.common.base.Charsets;
import com.virjar.hermes.hermesadmin.dao.TargetAppMapper;
import com.virjar.hermes.hermesadmin.model.CommonRes;
import com.virjar.hermes.hermesadmin.model.TargetApp;
import com.virjar.hermes.hermesadmin.util.CommonUtil;
import com.virjar.hermes.hermesadmin.util.Constant;
import com.virjar.hermes.hermesadmin.util.ReturnUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.dongliu.apk.parser.bean.ApkMeta;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * Created by virjar on 2018/9/5.<br>
 * 管理目标apk,如微信、支付宝、微视等
 */
@Slf4j
@RestController
@RequestMapping("/targetApp")
public class TargetAppController {
    //获取上传的文件夹，具体路径参考application.properties中的配置
    @Value("${web.upload-path}")
    private String uploadPath;

    @Resource
    private TargetAppMapper targetAppMapper;

    /**
     * @param file 上传一个apk
     */
    @ApiOperation(value = "上传apk文件", notes = "这个apk,是目标apk,也就是将要被hook的APK,apk大小限制,不超过500M")
    @PostMapping(value = "/upload")
    @ResponseBody
    public CommonRes<String> upload(@RequestParam("targetAPK") MultipartFile file) {
        String srcFileName = file.getOriginalFilename();
        if (!StringUtils.endsWithIgnoreCase(srcFileName, ".apk")) {
            return ReturnUtil.failed("the upload file must has .apk suffix");
        }

        File dir = genAgentApkUploadPath();
        String filedName = DateTime.now().toString("yyyy_MM_dd") + "_" + System.currentTimeMillis() + "_" + Thread.currentThread().getId();
        File targetFile = new File(dir, filedName);
        log.info("save new agent apk file to :{}", targetFile.getAbsoluteFile());
        try {
            file.transferTo(targetFile);
        } catch (IOException e) {
            log.error("failed to save agent apk filed", e);
            return ReturnUtil.failed(e);
        }

        ApkMeta apkMeta = CommonUtil.parseApk(targetFile);

        if (StringUtils.equalsIgnoreCase(Constant.agentApkPackage, apkMeta.getPackageName())) {
            //只能上传agent的apk文件,其他文件,认为是不合法的
            return ReturnUtil.failed("target apk package can not  be: " + Constant.agentApkPackage);
        }

        //check passed ,to rename with package name
        String realFileName = apkMeta.getPackageName() + "_" + apkMeta.getVersionName() + "_" + apkMeta.getVersionCode() + ".apk";
        File realFilePath = new File(dir, realFileName);
        if (realFilePath.exists()) {
            targetFile.deleteOnExit();
            return ReturnUtil.failed("target version existed");
        }
        targetFile.renameTo(realFilePath);

        TargetApp targetApp = new TargetApp();
        targetApp.setVersion(apkMeta.getVersionName());
        targetApp.setVersionCode(apkMeta.getVersionCode().intValue());
        targetApp.setEnabled(true);
        targetApp.setSavePath(realFilePath.getAbsolutePath());
        targetApp.setAppPackage(apkMeta.getPackageName());
        targetApp.setName(apkMeta.getName());
        targetAppMapper.insertSelective(targetApp);
        return ReturnUtil.success("upload success");
    }


    private File genAgentApkUploadPath() {
        File dir = new File(uploadPath);
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw new RuntimeException("failed to create upload directory: " + dir.getAbsolutePath());
            }
        }

        File agentApk = new File(dir, "target_apk");
        if (agentApk.isDirectory()) {
            return agentApk;
        }
        if (!agentApk.mkdir()) {
            throw new RuntimeException("failed to create agent apk file upload directory: " + agentApk.getAbsolutePath());
        }
        return agentApk;
    }

    @ApiOperation(value = "下载某个targetApk", notes = "这个是给Android内的agent访问的,他可以通过这个接口下载到新的apk代码,并完成自更新")
    @GetMapping("/download")
    public ResponseEntity<InputStreamResource> downloadFile(@RequestParam("package") String apkPackage)
            throws IOException {

        TargetApp targetApp = targetAppMapper.findAvilable(apkPackage);
        if (targetApp == null) {
            throw new RuntimeException("can not find apk for package: " + apkPackage);
        }

        File savePath = new File(targetApp.getSavePath());
        if (!savePath.exists() || !savePath.isFile() || !savePath.canRead()) {
            throw new IllegalStateException("resource can not access");
        }
        FileSystemResource file = new FileSystemResource(savePath);
        HttpHeaders headers = new HttpHeaders();
        headers.setCacheControl("no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", "attachment; filename=" + new String((targetApp.getAppPackage() + "_" + targetApp.getVersionCode() + ".apk").getBytes(Charsets.UTF_8), Charsets.ISO_8859_1));
        headers.setPragma("no-cache");
        headers.setExpires(0);
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(file.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(file.getInputStream()));
    }

    @GetMapping("/list")
    @ApiOperation(value = "列表,分页显示所有的目标app")
    @ResponseBody
    public CommonRes<Page<TargetApp>> list(@PageableDefault Pageable pageable) {
        Page<TargetApp> page = new PageImpl<>(targetAppMapper.selectPage(pageable),
                pageable, targetAppMapper.selectCount());
        return ReturnUtil.success(page);
    }

}
