package com.virjar.hermes.hermesadmin.dao;

import com.virjar.hermes.hermesadmin.model.Device;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DeviceMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Device record);

    int insertSelective(Device record);

    Device selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Device record);

    int updateByPrimaryKey(Device record);

    Device selectByMac(@Param("mac") String mac);

    List<Device> findUnInstalledDevice(@Param("servicePackage") String servicePackage);

    List<Device> selectPage(Pageable pageable);

    long selectCount();
}