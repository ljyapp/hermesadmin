package com.virjar.hermes.hermesadmin.dao;

import com.virjar.hermes.hermesadmin.model.AgentApk;

import java.util.List;

public interface AgentApkMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AgentApk record);

    int insertSelective(AgentApk record);

    AgentApk selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AgentApk record);

    int updateByPrimaryKey(AgentApk record);

    List<AgentApk> recent();
}