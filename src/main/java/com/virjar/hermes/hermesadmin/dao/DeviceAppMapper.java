package com.virjar.hermes.hermesadmin.dao;

import com.virjar.hermes.hermesadmin.model.DeviceApp;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DeviceAppMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DeviceApp record);

    DeviceApp selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DeviceApp record);

    int updateByPrimaryKey(DeviceApp record);

    @Deprecated
    List<DeviceApp> selectByDeviceMac(@Param("mac") String deviceMac);

    List<DeviceApp> selectByMacOrService(@Param("mac") String deviceMac, @Param("servicePackage") String servicePackage);

}