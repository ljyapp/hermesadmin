package com.virjar.hermes.hermesadmin.dao;

import com.virjar.hermes.hermesadmin.model.Device;
import com.virjar.hermes.hermesadmin.model.TargetApp;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TargetAppMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TargetApp record);

    int insertSelective(TargetApp record);

    TargetApp selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TargetApp record);

    int updateByPrimaryKey(TargetApp record);

    TargetApp findAvilable(@Param("apkPackage") String apkPackage);

    List<TargetApp> selectPage(Pageable pageable);

    long selectCount();
}