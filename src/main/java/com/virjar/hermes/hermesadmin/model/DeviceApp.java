package com.virjar.hermes.hermesadmin.model;

public class DeviceApp {
    private Long id;

    private Long targetAppId;

    private Long deviceId;

    private Integer status;

    private String appPackage;

    private String deviceMac;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTargetAppId() {
        return targetAppId;
    }

    public void setTargetAppId(Long targetAppId) {
        this.targetAppId = targetAppId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage == null ? null : appPackage.trim();
    }

    public String getDeviceMac() {
        return deviceMac;
    }

    public void setDeviceMac(String deviceMac) {
        this.deviceMac = deviceMac == null ? null : deviceMac.trim();
    }
}