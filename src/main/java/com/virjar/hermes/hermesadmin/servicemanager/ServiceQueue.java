package com.virjar.hermes.hermesadmin.servicemanager;

/**
 * Created by virjar on 2018/9/6.
 */
public interface ServiceQueue {
    void online(String mac);

    void offline(String mac);

    String rollPoling();

    int macSize();
}
